import { t, Selector } from 'testcafe'
import { isMobilePlatform } from "../utils";

class MainPage {
    constructor () {
        this.navigationAnalyticsTitle = [
            //'apple home',
            'store',
            'mac',
            'ipad',
            'iphone',
            'watch',
            'airpods',
            'tv and home',
            'only on apple',
            'accessories',
            'support'
        ]

        this.mobileBtnNavigation = Selector('.ac-gn-menuicon')

        this.navigationList = Selector('ul').withAttribute('class', 'ac-gn-list')
        this.navigationItem = this.navigationList.find('.ac-gn-link')
        this.navigationItemByAnalyticsTitle = (title) => {
            return this.navigationItem.withAttribute('data-analytics-title', title)
        }
    }

    async followNavigationItemByAnalyticsTitle (title) {
        if (isMobilePlatform()) {
            await t.click(this.mobileBtnNavigation)
        }

        await t
            .expect(this.navigationItemByAnalyticsTitle(title).visible)
            .ok()
            .click(this.navigationItemByAnalyticsTitle(title))
    }

    async getNavigationItemHref (title) {
        return await this.navigationItemByAnalyticsTitle(title).getAttribute('href')
    }
}

export default new MainPage()

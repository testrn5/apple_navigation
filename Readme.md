# Apple navigation tests

**Copy env:**
````
cp .env.example .env
````

**Start tests in Chrome:**
````
npm run chrome-tests
````

**Start tests in Edge:**
````
npm run edge-tests
````

**Start tests on iPhone 12 Pro, iOS 14.4:**
````
npm run ios-tests
````

**Some folders:**

|  Folder |            Description             |
|--------:|:----------------------------------:|
|  allure |  Contains xmls for allure reports  |
| reports | Contains screenshots with failures |


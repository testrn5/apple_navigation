import { t, RequestLogger } from "testcafe"
import main_page from "../pages/main_page"
import {
    getUrl,
    isLoggerContainsSuccessRequest
} from "../utils"

const url = process.env.HOST
const regUrl = new RegExp(url)

const logger = RequestLogger([
    { regUrl, method: 'get' },
], {
    logResponseHeaders: true,
    logResponseBody:    true
})

fixture('Navigation items')
    .page(url)
    .requestHooks(logger)

test('Check navigation items', async t => {
    for (const title of main_page.navigationAnalyticsTitle) {
        const urlToCheck = getUrl(await main_page.getNavigationItemHref(title))
        await main_page.followNavigationItemByAnalyticsTitle(title)

        const contains = isLoggerContainsSuccessRequest(logger.requests, urlToCheck)
        await t.expect(contains).ok()
        logger.clear()

        await t.navigateTo(getUrl('/'))
    }
})

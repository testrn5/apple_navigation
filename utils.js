import { t } from "testcafe"

export function getUrl(endpoint = '') {
    if (endpoint.startsWith('http')) {
        return endpoint
    }

    return `${process.env.HOST}${endpoint}`
}

export function isMobilePlatform() {
    return t.browser.platform === 'mobile'
}

const parseLocation = function (location, uri) {
    if (location.startsWith('http')) {
        return location
    }

    const url = new URL(uri)
    return `${url.protocol}//${url.hostname}${location}`
}

export function isLoggerContainsSuccessRequest(reqs, url) {
    for (const req of reqs) {
        if (req.request.url === url && req.response.statusCode === 200) {
            return true
        }

        if (req.request.url === url && req.response.headers.location !== undefined) {
            return isLoggerContainsSuccessRequest(reqs, parseLocation(req.response.headers.location, url))
        }
    }

    return false
}
